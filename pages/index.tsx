import React from "react";
import { IParallax, Parallax, ParallaxLayer } from "@react-spring/parallax";
import Image from "next/image";
import browserContentOne from '../public/browser-content-one.png'

import styles from "styles/Home.module.scss";

export default function Home() {
  const alignCenter = { display: "flex", alignItems: "center" };
  const parallaxRef = React.useRef<IParallax>(null!);

  return (
    <div className={styles.main}>
      <div className={styles.background} />
      
      <Parallax pages={12} ref={parallaxRef}>
        <ParallaxLayer
          offset={0}
          speed={0.5}
          style={{ ...alignCenter, justifyContent: "center" }}
        >
          <p className={styles.scrollText}>Scroll down</p>
        </ParallaxLayer>

        <ParallaxLayer
          sticky={{ start: 1, end: 8 }}
          className={styles.layer}
        />

        <ParallaxLayer
          sticky={{ start: 1, end: 2 }}
          style={{ ...alignCenter, justifyContent: "center" }}
        >
          <Image
            alt="Browser Frame"
            src={browserContentOne}
            placeholder="blur"
            quality={100}
            sizes="100vh"
            style={{
              objectFit: 'cover',
            }}
          />
        </ParallaxLayer>

        <ParallaxLayer
          sticky={{ start: 3, end: 4 }}
          style={{ ...alignCenter, justifyContent: "center" }}
        >
          <Image
            alt="Browser Frame"
            src={browserContentOne}
            placeholder="blur"
            quality={100}
            sizes="100vh"
            style={{
              objectFit: 'cover',
            }}
          />
        </ParallaxLayer>

        <ParallaxLayer
          sticky={{ start: 5, end: 6 }}
          style={{ ...alignCenter, justifyContent: "center" }}
        >
          <Image
            alt="Browser Frame"
            src={browserContentOne}
            placeholder="blur"
            quality={100}
            sizes="100vh"
            style={{
              objectFit: 'cover',
            }}
          />
        </ParallaxLayer>

        <ParallaxLayer
          sticky={{ start: 7, end: 8 }}
          style={{ ...alignCenter, justifyContent: "center" }}
        >
          <Image
            alt="Browser Frame"
            src={browserContentOne}
            placeholder="blur"
            quality={100}
            sizes="100vh"
            style={{
              objectFit: 'cover',
            }}
          />
        </ParallaxLayer>
        
        <ParallaxLayer
          sticky={{ start: 10, end: 12 }}
          style={{ ...alignCenter, justifyContent: "center" }}
        >
          <p className={styles.scrollText}>End</p>
        </ParallaxLayer>
      </Parallax>
    </div>
  );
}
